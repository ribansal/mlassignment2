import os, sys
path = os.path.join(sys.path[0],'lib')
sys.path.append(path)

import lib
#import lib.queens_opt as queens
import lib.sa as sa
import lib.randhill as randhill
import lib.mimic as mimic
import lib.ga as ga


def main(runtype, method,n,ntype):

    grid = {}

    if(ntype == "long"):
        grid['max_iters'] = [5,10,20, 50, 100, 400, 1000,]#[50,100,400,1000]
        grid['max_attempts'] = [50]
    else:
        grid['max_iters'] = [5,10,20,50,100,400,1000]#[50,100,400,1000]
        grid['max_attempts'] = [20]

    if((runtype=="queen") and (method=='sa')): 
        sa.run_queen_sa(runtype,method,n,grid)
    elif((runtype=="queen") and (method=='randhill')): 
        randhill.run_queen_randhill(runtype,method,n,grid)
    elif((runtype=="queen") and (method=='mimic')): 
        mimic.run_queen_mimic(runtype,method,n,grid)
    elif((runtype=="queen") and (method=='ga')): 
        ga.run_queen_ga(runtype,method,n,grid)
    elif((runtype=="knapsack") and (method=='randhill')): 
        randhill.run_knapsack_randhill(runtype,method,n,grid)
    elif((runtype=="knapsack") and (method=='sa')): 
        sa.run_knapsack_sa(runtype,method,n,grid)
    elif((runtype=="knapsack") and (method=='mimic')): 
        mimic.run_knapsack_mimic(runtype,method,n,grid)
    elif((runtype=="knapsack") and (method=='ga')):
        ga.run_knapsack_ga(runtype,method,n,grid)
    elif((runtype=="flipflop") and (method=='randhill')): 
        randhill.run_flipflop_randhill(runtype,method,n,grid)
    elif((runtype=="flipflop") and (method=='sa')): 
        sa.run_flipflop_sa(runtype,method,n,grid)
    elif((runtype=="flipflop") and (method=='mimic')): 
        mimic.run_flipflop_mimic(runtype,method,n,grid)
    elif((runtype=="flipflop") and (method=='ga')): 
        ga.run_flipflop_ga(runtype,method,n,grid)
    elif((runtype=="maxkcolor") and (method=='randhill')): 
        randhill.run_maxkcolor_randhill(runtype,method,n,grid)
    elif((runtype=="maxkcolor") and (method=='sa')): 
        sa.run_maxkcolor_sa(runtype,method,n,grid)
    elif((runtype=="maxkcolor") and (method=='ga')): 
        ga.run_maxkcolor_ga(runtype,method,n,grid)
    elif((runtype=="maxkcolor") and (method=='mimic')): 
        mimic.run_maxkcolor_mimic(runtype,method,n,grid)
    elif((runtype=="tsp") and (method=='randhill')): 
        randhill.run_tsp_randhill(runtype,method,n,grid)
    elif((runtype=="tsp") and (method=='sa')): 
        sa.run_tsp_sa(runtype,method,n,grid)
    elif((runtype=="tsp") and (method=='ga')): 
        ga.run_tsp_ga(runtype,method,n,grid)
    elif((runtype=="tsp") and (method=='mimic')): 
        mimic.run_tsp_mimic(runtype,method,n,grid)
    else:
        print("invalid input")


if __name__ == '__main__':
    runtype = sys.argv[1] #[queen,knapsack,flipflop, maxkcolor, tsp]
    method = sys.argv[2]  #[randhill sa mimic ga]
    ncase = int(sys.argv[3]) #size of problem
    ntype = sys.argv[4] # type short or long
    
    if(runtype=='all'):
        runtype = ['queen','knapsack','flipflop','maxkcolor', 'tsp']
    else : runtype = [runtype]
    if method == 'all':
        method = ['randhill','sa','ga','mimic']
    else : method = [method]

    for ri in runtype:
        for mi in method:
            print('running runtype %s method %s ntype %s n %s'%(ri,mi,ntype,ncase))
            main(ri,mi,ncase,ntype)
