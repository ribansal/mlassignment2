#from optalgos import *
import mlrose
import algorithm
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt
import os
import plot
import tsp_mod
import numpy as np


def run_queen_randhill(runtype,method,n,grid):
    folder = "%s_%s_n_%s_data"%(runtype,method,n)
    os.system("rm -rf %s && mkdir %s"%(folder,folder))
    os.chdir("%s"%folder)
    problem = algorithm.set_discrete_opt(n,mlrose.CustomFitness(algorithm.queens_max))
    #sending initial grid
    run_randhill_n(n,runtype,method,problem,grid,[0,n*(n+1)/2])
    os.chdir("../")

def run_knapsack_randhill(runtype,method,n,grid):
    folder = "%s_%s_n_%s_data"%(runtype,method,n)
    os.system("rm -rf %s && mkdir %s"%(folder,folder))
    os.chdir("%s"%folder)
    weights,values = algorithm.generate_knapsack(n,25,10)
    inp = {'weights':weights,'values':values}
    df = pd.DataFrame(inp) 
    print(df)
    df.to_csv("input.txt",sep="\t",float_format='%.1f')
    function = mlrose.Knapsack(weights, values, max_weight_pct=0.6)
    problem = algorithm.set_knapsack_discrete_opt(n,function,4)

    run_randhill_n(n,runtype,method,problem,grid,[0,n*12])
    os.chdir("../")

def run_flipflop_randhill(runtype,method,n,grid):
    folder = "%s_%s_n_%s_data"%(runtype,method,n)
    os.system("rm -rf %s && mkdir %s"%(folder,folder))
    os.chdir("%s"%folder)
    problem = algorithm.set_knapsack_discrete_opt(n,mlrose.FlipFlop(),4)
    run_randhill_n(n,runtype,method,problem,grid,[0,n+1])
    os.chdir("../")

def run_maxkcolor_randhill(runtype,method,n,grid):
    folder = "%s_%s_n_%s_data"%(runtype,method,n)
    os.system("rm -rf %s && mkdir %s"%(folder,folder))
    os.chdir("%s"%folder)
    edges,state,k = algorithm.read_maxcolor_input(n)
    edges = algorithm.preprocess_k_color(edges)
    dict = {}
    dict['edges'] = edges 
    fitness_funct = mlrose.CustomFitness(algorithm.max_k_color_max,**dict)
    problem = algorithm.set_knapsack_discrete_opt(len(state),fitness_funct,k)
    #sending initial grid
    run_randhill_n(n,runtype,method,problem,grid,[0,len(edges)])
    os.chdir("../")

def run_tsp_randhill(runtype,method,n,grid):
    folder = "%s_%s_n_%s_data"%(runtype,method,n)
    os.system("rm -rf %s && mkdir %s"%(folder,folder))
    os.chdir("%s"%folder)
    #dists = [(0, 1, 3), (0, 2, 5), (0, 3, 1), (0, 4, 7), (1, 3, 6),(4, 1, 9), (2, 3, 8), (2, 4, 2), (3, 2, 8), (3, 4, 4)]
    dists = algorithm.get_tsp_data(n)
    #print(dists)
    fitness_funct = tsp_mod.TravellingSalesMod(distances=dists)
    #fitness_funct = mlrose.TravellingSales(distances=dists)
    problem = mlrose.TSPOpt(length = n, fitness_fn = fitness_funct, maximize=True)
    run_randhill_n_state(n,runtype,method,problem,grid,[0,-20*n])
    os.chdir("../")

def run_randhill_n(n,runtype,method,problem,grid,ylim):
    print("running %s with %s"%(runtype,method))
    #n=50
    #problem = algorithm.set_discrete_opt(n,mlrose.CustomFitness(algorithm.queens_max))
    
    #init_state = np.arange(0,n)
    init_state = [0]*n
    #initial parameters for simulated annealing
    params = {}
    params['max_attempts'] = 20
    params['max_iters'] = 100
    params['restarts'] = 0
    params['init_state'] = init_state
    params['random_state'] = 1
    params['curve'] = True

    #grid dictionary for parameter search space
    #grid = {}
    #grid['max_iters'] = [5,20,50,100,200,500,1000]
    #grid['max_attempts'] = [5,10,20,50,100]
    grid['restarts'] = [0,10,20]
    #grid['restarts'] = [2]
    keys = ['max_attempts','max_iters', 'restarts']   
    space = algorithm.create_arg_combinations(grid,keys)
    df = algorithm.run_randhill_grid(keys,space,problem,params,4)
    #print(df)
    #plot.plot_randhill_results(df,grid,ylim)

    df.to_csv("%s_%s.csv"%(runtype,method),sep=",",float_format='%.4f')
    df = df.drop(["fitness_curve"],axis=1)
    df = df.drop(["best_state"],axis=1)
    df.to_csv("%s_%s.log"%(runtype,method),sep="\t",float_format='%.4f')

def run_randhill_n_state(n,runtype,method,problem,grid,ylim):
    print("running %s with %s"%(runtype,method))
    #n=50
    #problem = algorithm.set_discrete_opt(n,mlrose.CustomFitness(algorithm.queens_max))
    
    init_state = np.arange(0,n)
    #print(init_state)
    #init_state = [0]*n
    #initial parameters for simulated annealing
    params = {}
    params['max_attempts'] = 20
    params['max_iters'] = 100
    params['restarts'] = 0
    params['init_state'] = init_state
    params['random_state'] = 1
    params['curve'] = True

    #grid dictionary for parameter search space
    #grid = {}
    #grid['max_iters'] = [5,20,50,100,200,500,1000]
    #grid['max_attempts'] = [5,10,20,50,100]
    grid['restarts'] = [0,5,10,20]
    #grid['restarts'] = [2]
    keys = ['max_attempts','max_iters', 'restarts']   
    space = algorithm.create_arg_combinations(grid,keys)
    df = algorithm.run_randhill_grid(keys,space,problem,params,4)
    #print(df)
    #plot.plot_randhill_results(df,grid,ylim)

    df.to_csv("%s_%s.csv"%(runtype,method),sep=",",float_format='%.4f')
    df = df.drop(["fitness_curve"],axis=1)
    df = df.drop(["best_state"],axis=1)
    df.to_csv("%s_%s.log"%(runtype,method),sep="\t",float_format='%.4f')





