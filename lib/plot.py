import mlrose
import algorithm
import pandas as pd
import matplotlib.pyplot as plt
import os

def plot_randhill_results(df,grid,ylim):

    #ploting results for geometric
    restarts = grid["restarts"]
    restarts = [ round(elem, 0) for elem in restarts ]
    max_iters = grid["max_iters"]
    max_attempts = grid["max_attempts"]
    #plot for each max_attempts
    for ai in max_attempts:
        df1 = df[df.max_attempts == ai]
        zcase = {}
        zcase["vaname"] = 'restarts'
        zcase["vvalue"] = restarts
        zcase['vlabel'] = 'n_restart'
        zcase['title'] = "max_attempts-%s"%ai
        zcase['ylim'] = ylim
        algorithm.plot_df(df1,'max_iters','best_fitness',zcase,'')

        for bi in max_iters:
        #plotting time
            df2 = df1[df1.max_iters==bi]
            zcase = {}
            zcase["vaname"] = 'restarts'
            zcase["vvalue"] = restarts
            zcase['vlabel'] = 'n_restart'
            zcase['title'] = "max_attempts-%s,max_iters-%s"%(ai,bi)
            zcase['ylim'] = ylim
            algorithm.plot_df(df2,'runtime','best_fitness',zcase,'')



def plot_sa_results(df,grid,ylim):

    #ploting results for geometric
    schedule = grid["schedule"][0]
    decayrate = grid["decayrate"]
    decayrate = [ round(elem, 4) for elem in decayrate ]
    max_iters = grid["max_iters"]
    max_iters = grid["max_iters"]
    max_attempts = grid["max_attempts"]
    df = df[df.schedule == schedule]
    #plot for each max_attempts
    for ai in max_attempts:
        df1 = df[df.max_attempts == ai]
        zcase = {}
        zcase["vaname"] = 'decayrate'
        zcase["vvalue"] = decayrate
        zcase['vlabel'] = 'Decayrate'
        zcase['title'] = "max_attempts-%s"%ai
        zcase['ylim'] = ylim
        algorithm.plot_df(df1,'max_iters','best_fitness',zcase,schedule)

    for ai in max_attempts:
        for bi in max_iters:
            #plotting time
            df1 = df[(df.max_attempts == ai)]
            df1 = df1[df1.max_iters == bi]
            zcase = {}
            zcase["vaname"] = 'decayrate'
            zcase["vvalue"] = decayrate
            zcase['vlabel'] = 'Decayrate'
            zcase['title'] = "max_attempts-%s,max_iters-%s"%(ai,bi)
            zcase['ylim'] = ylim
            algorithm.plot_df(df1,'runtime','best_fitness',zcase,schedule)



def plot_mimic_results(df,grid,ylim):

    #ploting results for geometric
    pop_size = grid["pop_size"]
    keep_pct = grid['keep_pct']
    keep_pct = [ round(elem, 2) for elem in keep_pct ]
    max_iters = grid["max_iters"]
    max_attempts = grid["max_attempts"]
    #plot for each max_attempts
    for pi in keep_pct:
        for ai in max_attempts:
            df1 = df[(df.max_attempts == ai) & (df.keep_pct==pi)]
            zcase = {}
            zcase["vaname"] = 'pop_size'
            zcase["vvalue"] = pop_size
            zcase['vlabel'] = 'pop_size'
            zcase['title'] = "max_attempts-%s,keep_pct-%s"%(ai,pi)
            zcase['ylim'] = ylim
            algorithm.plot_df(df1,'max_iters','best_fitness',zcase,'')

            for bi in max_iters:
            #plotting time
                df2 = df1[df1.max_iters==bi]
                zcase = {}
                zcase["vaname"] = 'pop_size'
                zcase["vvalue"] = pop_size
                zcase['vlabel'] = 'pop_size'
                zcase['title'] = "max_attempts-%s,keep_pct-%s,max_iters-%s"%(ai,pi,bi)
                zcase['ylim'] = ylim
                algorithm.plot_df(df2,'runtime','best_fitness',zcase,'')



def plot_ga_results(df,grid,ylim):

    #ploting results for geometric
    pop_size = grid["pop_size"]
    mutation_prob = grid['mutation_prob']
    mutation_prob = [ round(elem, 2) for elem in mutation_prob ]
    max_iters = grid["max_iters"]
    max_attempts = grid["max_attempts"]
    #plot for each max_attempts
    for pi in mutation_prob:
        for ai in max_attempts:
            df1 = df[(df.max_attempts == ai) & (df.mutation_prob==pi)]
            zcase = {}
            zcase["vaname"] = 'pop_size'
            zcase["vvalue"] = pop_size
            zcase['vlabel'] = 'pop_size'
            zcase['title'] = "max_attempts-%s,mutation_prob-%s"%(ai,pi)
            zcase['ylim'] = ylim
            algorithm.plot_df(df1,'max_iters','best_fitness',zcase,'')

            for bi in max_iters:
            #plotting time
                df2 = df1[df1.max_iters==bi]
                zcase = {}
                zcase["vaname"] = 'pop_size'
                zcase["vvalue"] = pop_size
                zcase['vlabel'] = 'pop_size'
                zcase['title'] = "max_attempts-%s,mutation_prob-%s,max_iters-%s"%(ai,pi,bi)
                zcase['ylim'] = ylim
                algorithm.plot_df(df2,'runtime','best_fitness',zcase,'')