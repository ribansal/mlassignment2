import mlrose
import sys
import numpy as np
import matplotlib.pyplot as plt
import time
import itertools as it
import pandas as pd
import ga_mod
import multiprocessing as mp

#mlrose library taken from https://github.com/gkhayes/mlrose/tree/master/mlrose

def set_discrete_opt(n,fitness_funct):
    problem = mlrose.DiscreteOpt(length=n, fitness_fn=fitness_funct, maximize=True, max_val=n)
    return problem

def set_knapsack_discrete_opt(n,fitness_funct,maxval):
    problem = mlrose.DiscreteOpt(length=n, fitness_fn=fitness_funct, maximize=True, max_val=maxval)
    return problem

def simulated_annealing(problem,params):

    itern_start_time = time.perf_counter()  
    best_state, best_fitness, fitness_curve = mlrose.simulated_annealing(problem,**params)
    itern_time = time.perf_counter() - itern_start_time

    return best_state, best_fitness, fitness_curve, itern_time

def random_hill_climb(problem,params):
    itern_start_time = time.perf_counter()  
    best_state, best_fitness, fitness_curve = mlrose.random_hill_climb(problem,**params)
    itern_time = time.perf_counter() - itern_start_time

    return best_state, best_fitness, fitness_curve, itern_time

def mimic(problem,params):
    itern_start_time = time.perf_counter()  
    best_state, best_fitness, fitness_curve = mlrose.mimic(problem,**params)
    itern_time = time.perf_counter() - itern_start_time

    return best_state, best_fitness, fitness_curve, itern_time

def ga(problem,params):
    itern_start_time = time.perf_counter()  
    #best_state, best_fitness, fitness_curve = mlrose.genetic_alg(problem,**params)
    best_state, best_fitness, fitness_curve = ga_mod.genetic_alg(problem,**params)
    itern_time = time.perf_counter() - itern_start_time

    return best_state, best_fitness, fitness_curve, itern_time

def queens_max(state):

    # Initialize counter
    fitness_cnt = 0

    # For all pairs of queens
    for i in range(len(state) - 1):
        for j in range(i + 1, len(state)):

            # Check for horizontal, diagonal-up and diagonal-down attacks
            if (state[j] != state[i]) \
            and (state[j] != state[i] + (j - i)) \
            and (state[j] != state[i] - (j - i)):

                # If no attacks, then increment counter
                fitness_cnt += 1

    return fitness_cnt

def max_k_color_max(state,**param):
    edges = param['edges']

    #print(len(edges))
    edges = list({tuple(sorted(edge)) for edge in edges})
    #print(len(edges))
    #print(len(state))
    fitness = 0

    for i in range(len(edges)):
        # Check for adjacent nodes of the same color
        if state[edges[i][0]] != state[edges[i][1]]:
            fitness += 1

    return fitness



def create_arg_combinations(grid,keys):
    space = []
    for gtype in keys:
        space.append(grid[gtype])
    arg_product = list(it.product(*space))
    return arg_product



def run_randhill_grid(keys,space,problem,params,njobs=4):

    colname = ['max_attempts','max_iters','restarts','best_fitness','runtime','niter','best_state','fitness_curve']
    df = pd.DataFrame(columns=colname)

    #pool = mp.Pool(4)
    #results = [pool.apply(run_randhill_parallel, args=(keys,si,problem,params)) for si in space]
    #pool.close()

    pool = mp.Pool(njobs)
    results = [pool.apply_async(run_randhill_parallel, args=(keys,si,problem,params)) for si in space]
    pool.close()
    pool.join() 
    results = [r.get() for r in results]

    for ri in results:
        df = df.append(ri,ignore_index=True)


    return df


def run_sa_grid(keys,space,problem,params,njobs=4):

    colname = ['schedule', 'decayrate', 'max_attempts','max_iters','best_fitness','runtime','niter','best_state','fitness_curve']
    df = pd.DataFrame(columns=colname)

    #pool = mp.Pool(4)
    #results = [pool.apply(run_sa_parallel, args=(keys,si,problem,params)) for si in space]
    #pool.close()

    pool = mp.Pool(njobs)
    results = [pool.apply_async(run_sa_parallel, args=(keys,si,problem,params)) for si in space]
    pool.close()
    pool.join() 
    results = [r.get() for r in results]

    for ri in results:
        df = df.append(ri,ignore_index=True)

    return df


def run_mimic_grid(keys,space,problem,params,njobs=4):

    colname = ['max_attempts','max_iters','pop_size','keep_pct','best_fitness','runtime','niter','best_state','fitness_curve']
    df = pd.DataFrame(columns=colname)


    #pool = mp.Pool(4)
    #results = [pool.apply(run_mimic_parallel, args=(keys,si,problem,params)) for si in space]
    #pool.close()

    pool = mp.Pool(njobs)
    results = [pool.apply_async(run_mimic_parallel, args=(keys,si,problem,params)) for si in space]
    pool.close()
    pool.join() 
    results = [r.get() for r in results]

    for ri in results:
        df = df.append(ri,ignore_index=True)

    return df


def run_ga_grid(keys,space,problem,params,njobs=4):

    colname = keys + ['best_fitness','runtime','niter','best_state','fitness_curve']
    df = pd.DataFrame(columns=colname)

    #pool = mp.Pool(4)
    #results = [pool.apply(run_ga_parallel, args=(keys,si,problem,params)) for si in space]
    #pool.close()


    pool = mp.Pool(njobs)
    results = [pool.apply_async(run_ga_parallel, args=(keys,si,problem,params)) for si in space]
    pool.close()
    pool.join() 
    results = [r.get() for r in results]

    for ri in results:
        df = df.append(ri,ignore_index=True)

    return df


def get_decay_object(decaytype,decayrate):
    #print(decaytype,decayrate)
    if(decaytype == 'geometric'): return mlrose.GeomDecay(decay=decayrate)
    if(decaytype == 'airthmatic'): return mlrose.ArithDecay(decay=decayrate)
    if(decaytype == 'exponential'): return mlrose.ExpDecay(exp_const=decayrate)
    print("incorrect decay type")
    sys.exit()



def plot_df(df,xcol,ycol,zcase,prefix):
    #print(zcase)
    
    fig = plt.figure()

    for vi in zcase["vvalue"]:
        #print(vi)
        df1 = df.query('%s == %s'%(zcase['vaname'],vi))
        x = df1[xcol].values
        y = df1[ycol].values
        #print(x,y)

        plt.plot(x, y, linestyle='-', marker='s',markersize=5, label='%s-%s'%(zcase["vlabel"],vi))

    plt.grid()
    plt.xlabel(xcol)
    plt.ylabel(ycol)
    plt.legend(loc='best')
    plt.title(zcase["title"])
    #plt.xlim([0.00001, 0.01])
    if'ylim' in zcase.keys():
        plt.ylim(zcase['ylim'])
    #plt.show()
    filename = "%s_%s_%s_%s.png"%(prefix,xcol,ycol,zcase['title'])
    plt.savefig(filename,dpi=140)
    plt.close(fig)


def generate_knapsack(number_of_items_types,max_weight_per_item,max_value_per_item):
    np.random.seed(123)
    weights = 1 + np.random.randint(max_weight_per_item, size=number_of_items_types)
    values = 1 + np.random.randint(max_value_per_item, size=number_of_items_types)

    return weights,values


def run_randhill_parallel(keys,si,problem,params):
    #print(si)
    params['max_attempts'] = si[keys.index('max_attempts')]
    params['max_iters'] = si[keys.index('max_iters')]
    params['restarts'] = si[keys.index('restarts')]
    best_state, best_fitness, fitness_curve, runtime = random_hill_climb(problem,params)
    iter_data = {
            'max_attempts':params['max_attempts'],
            'max_iters':params['max_iters'],
            'restarts':params['restarts'],
            'best_state': best_state,
            'best_fitness': best_fitness,
            'fitness_curve':fitness_curve,
            'niter' : len(fitness_curve),
            'runtime':runtime  
        }
    return iter_data

def run_sa_parallel(keys,si,problem,params):
    decaytype = si[keys.index('schedule')]
    decayrate = si[keys.index('decayrate')]
    params['schedule'] = get_decay_object(decaytype,decayrate)
    params['max_attempts'] = si[keys.index('max_attempts')]
    params['max_iters'] = si[keys.index('max_iters')]
    best_state, best_fitness, fitness_curve, runtime = simulated_annealing(problem,params)
    iter_data = {
            'schedule':decaytype,
            'decayrate':decayrate,
            'max_attempts':params['max_attempts'],
            'max_iters':params['max_iters'],
            'best_state': best_state,
            'best_fitness': best_fitness,
            'fitness_curve':fitness_curve,
            'niter' : len(fitness_curve),
            'runtime':runtime  
        }
    return iter_data

def run_ga_parallel(keys,si,problem,params):

    for ki in keys:
        params[ki] = si[keys.index(ki)]

    best_state, best_fitness, fitness_curve, runtime = ga(problem,params)
    iter_data = {}
    for ki in keys:
        iter_data[ki] = params[ki]
    iter_data['best_state'] = best_state
    iter_data['best_fitness'] =  best_fitness
    iter_data['fitness_curve'] = fitness_curve
    iter_data['runtime'] = runtime 
    iter_data['niter'] = len(fitness_curve)
    return iter_data

def run_mimic_parallel(keys,si,problem,params):

    params['max_attempts'] = si[keys.index('max_attempts')]
    params['max_iters'] = si[keys.index('max_iters')]
    params['pop_size'] = si[keys.index('pop_size')]
    params['keep_pct'] = si[keys.index('keep_pct')]
    best_state, best_fitness, fitness_curve, runtime = mimic(problem,params)
    iter_data = {
            'max_attempts':params['max_attempts'],
            'max_iters':params['max_iters'],
            'pop_size':params['pop_size'],
            'keep_pct':params['keep_pct'],
            'best_state': best_state,
            'best_fitness': best_fitness,
            'fitness_curve':fitness_curve,
            'niter' : len(fitness_curve),
            'runtime':runtime  
        }
    return iter_data

def preprocess_k_color(edges):
    for i in range(len(edges)):
        edges[i] = (edges[i][0]-1,edges[i][1]-1)
    return(edges)


def read_maxcolor_input(n):
    filename = "../input_maxkolor_n_%s.txt"%n
    #print(filename)
    fid = open(filename,'r')
    data = {}
    while(True):
        line = fid.readline().strip()
        if line == '': break
        line = line.split()
        data[line[0]] = line[1]
    if int(data['n']) ==n:
        state = data['state'].split(',')
        state = [int(si) for si in state]
        edges = []
        for t in data['edges'].strip().split('('):
            t = t.strip('),')
            if t != '':
                a,b = t.split(',')
                edges.append((int(a),int(b)))
        k = int(data['k'])
        return edges,state,k
    else : 
        print('wrong number of entries')
        sys.exit()

def get_tsp_data(n):
    dists = []
    data = np.genfromtxt('../input_tsp_n_%s.txt'%n)
    for i in range(n):
        for j in range(i+1,n):
            dists.append((i,j,data[i,j]))
    return dists
