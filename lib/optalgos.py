import mlrose
import sys
import numpy as np
import matplotlib.pyplot as plt
import time
import itertools as it
import pandas as pd

def run_optalgo(algorithm, problem_arg, init_state, max_attempts, max_iterations, make_curve, seed_val, **args):
    np.random.seed(seed_val)
    full_iter_data=[]
    arg_list = [([(k, v) for v in vs]) for (k, (n, vs)) in args.items() if vs is not None]
    print(arg_list)
    arg_product = list(it.product(*arg_list))
    print(arg_product)
    start_run_time = time.perf_counter()
    for arg_value in arg_product:
        print(arg_value)
        for niter in max_iterations:
            arg_dict = dict(arg_value)
            print(arg_dict)
            print(f'*** Iteration START - params: {arg_dict}')
            itern_start_time = time.perf_counter()        
            cur_state, cur_fitness, cur_fitness_curve = algorithm(problem=problem_arg,
                                        init_state=init_state,
                                        max_attempts=max_attempts,
                                        curve=make_curve,
                                        random_state=seed_val,
                                        max_iters=niter,
                                        **arg_dict)
            itern_time = time.perf_counter() - itern_start_time
            
            iter_data = {
                'Iterations':niter,
                'fitness': cur_fitness,
                'time': itern_time,
                'state':cur_state,
                'fitness curve':cur_fitness_curve ,
                'args':arg_dict               
            }

            full_iter_data.append(iter_data)

        print(f'*** Iteration END - params: {arg_dict}')
    end_run_time = time.perf_counter()
    print(f'Run time: {end_run_time - start_run_time}')
    df_iter_data = pd.DataFrame(full_iter_data)
    df_iter_data.to_csv("./queens_out.csv")

def simul_annealing(problem, schedule, maxattempts, maxiters, initstate, log):
    best_state, best_fitness, fitness_curve = mlrose.simulated_annealing(problem, schedule = schedule,
                                                        max_attempts = maxattempts, max_iters = maxiters,
                                                        init_state = initstate, random_state = 10, curve=True) 
    print(best_state)

    print(best_fitness)
    print(fitness_curve)
    print(len(fitness_curve),fitness_curve[0])
    log.write("\nbest state is {}".format(str(best_state)))
    log.write("\nbest fitness is {}".format(str(best_fitness)))
    log.write("\nfitness curve is {}".format(str(fitness_curve)))
    iterlist = list(range(maxiters))
    plt.plot(iterlist, fitness_curve, color='red', marker='o', markersize=5, 
                label='fitness')
    plt.grid()
    plt.xlabel("Number of iterations")
    plt.ylabel('Fitness')
    plt.legend(loc='best')
    #plt.xlim([0.00001, 0.01])
    #plt.show()
    filename = "queen_SA.png"
    plt.savefig(filename, bbox_inches='tight')
    log.close()

def randomized_hill_climbing(problem, schedule, maximum_attempts, maximum_iters, initial_state, log):

    best_state, best_fitness, fitness_curve = random_hill_climb(problem, max_attempts = maximum_attempts, max_iters = maximum_iters, 
                                                restarts=0, init_state = initial_state, random_state = 10, curve=True)
    print(best_state)

    print(best_fitness)
    print(fitness_curve)
    print(len(fitness_curve),fitness_curve[0])
    log.write("\nbest state is {}".format(str(best_state)))
    log.write("\nbest fitness is {}".format(str(best_fitness)))
    log.write("\nfitness curve is {}".format(str(fitness_curve)))
    iterlist = list(range(maxiters))
    plt.plot(iterlist, fitness_curve, color='red', marker='o', markersize=5, 
                label='fitness')
    plt.grid()
    plt.xlabel("Number of iterations")
    plt.ylabel('Fitness')
    plt.legend(loc='best')
    #plt.xlim([0.00001, 0.01])
    #plt.show()
    filename = "queen_SA.png"
    plt.savefig(filename, bbox_inches='tight')
    log.close()

def mimic_impl(problem, schedule, maximum_attempts, maximum_iters, initial_state, log):

    best_state, best_fitness, fitness_curve = mimic(problem, pop_size=200, keep_pct=0.2, max_attempts=maximum_attempts, 
                                                max_iters=maximum_iters, curve=True, random_state=10)
    print(best_state)

    print(best_fitness)
    print(fitness_curve)
    print(len(fitness_curve),fitness_curve[0])
    log.write("\nbest state is {}".format(str(best_state)))
    log.write("\nbest fitness is {}".format(str(best_fitness)))
    log.write("\nfitness curve is {}".format(str(fitness_curve)))
    iterlist = list(range(maxiters))
    plt.plot(iterlist, fitness_curve, color='red', marker='o', markersize=5, 
                label='fitness')
    plt.grid()
    plt.xlabel("Number of iterations")
    plt.ylabel('Fitness')
    plt.legend(loc='best')
    #plt.xlim([0.00001, 0.01])
    #plt.show()
    filename = "queen_SA.png"
    plt.savefig(filename, bbox_inches='tight')
    log.close()

