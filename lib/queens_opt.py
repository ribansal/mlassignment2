from optalgos import *

'''

    Define a fitness function object.
    Define an optimization problem object.
    Select and run a randomized optimization algorithm.
'''
# Define alternative N-Queens fitness function for maximization problem
def queens_max(state):

    # Initialize counter
    fitness_cnt = 0

    # For all pairs of queens
    for i in range(len(state) - 1):
        for j in range(i + 1, len(state)):

            # Check for horizontal, diagonal-up and diagonal-down attacks
            if (state[j] != state[i]) \
            and (state[j] != state[i] + (j - i)) \
            and (state[j] != state[i] - (j - i)):

                # If no attacks, then increment counter
                fitness_cnt += 1

    return fitness_cnt

def queen_generic():
    fitness_cust = mlrose.CustomFitness(queens_max)
    n=12
    problem_arg = mlrose.DiscreteOpt(length=n, fitness_fn=fitness_cust, maximize=True, max_val=n)

    init_state = np.array([0,1,2,3,4,5,6,7,8,9,10,11])
    max_attempts=10
    make_curve=True
    seed_val=50
    decay_type_list = [mlrose.GeomDecay, mlrose.ExpDecay, mlrose.ArithDecay]
    expdecay_range_list = list(np.arange(0.005,0.1,0.005))
    arithdecay_range_list = list(np.arange(0.0001,0.1,0.01))
    geomdecay_range_list = list(np.arange(0.1,1.01,0.09))
    max_iterations = [10,100,200,300,400,500,1000]
    decay_obj_list = [decay_type_list[0](decay=t) for t in geomdecay_range_list] + \
                    [decay_type_list[1](exp_const=t) for t in expdecay_range_list] + \
                    [decay_type_list[2](decay=t) for t in arithdecay_range_list]
    algorithm=mlrose.simulated_annealing
    run_optalgo(algorithm, problem_arg, init_state, max_attempts, max_iterations, make_curve, seed_val, schedule=('Decay', decay_obj_list))


"""
def queen_solver_max():
    np.set_printoptions(threshold=sys.maxsize)
    log = open("queensolvermax.txt", "a")
    x='*'*80
    log.write(x+"\n")
    # Initialize custom fitness function object
    fitness_cust = mlrose.CustomFitness(queens_max)

    n = 12
    maxattempts=100
    maxiters=20
    problem = mlrose.DiscreteOpt(length=n, fitness_fn=fitness_cust, maximize=True, max_val=n)

    schedule = mlrose.ArithDecay()

    init_state = np.array([0,1,2,3,4,5,6,7,8,9,10,11]) #12,13,14,15,16,17,18,19])
    log.write("\ninit state is {}".format(str(init_state)))
    simul_annealing(problem, schedule, maxattempts, maxiters, init_state, log)

def queen_solver_min():

    fitness = mlrose.Queens()

    problem = mlrose.DiscreteOpt(length=12, fitness_fn=fitness, maximize=False, max_val=12)

    schedule = mlrose.ExpDecay()

    init_state = np.array([0,1,2,3,4,5,6,7,8,9,10,11])

    best_state, best_fitness = mlrose.simulated_annealing(problem, schedule = schedule,
                                                        max_attempts = 2000, max_iters = 1500,init_state = init_state, random_state = 10) 
    print(best_state)

    print(best_fitness)
"""
def main():
    queen_generic()
    #queen_solver_max()
    #queen_solver_min()
if __name__ == '__main__':
    main()