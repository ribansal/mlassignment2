from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score
import numpy as np
import pandas as pd
import mlrose
from sklearn.preprocessing import StandardScaler, OneHotEncoder
import time
import itertools as it
from sklearn.model_selection import StratifiedKFold,cross_validate
from sklearn.metrics import recall_score
import sklearn

def gridsearch(X_train, y_train, X_test,y_test, **args):
    scoring = ['accuracy','precision_macro', 'recall_macro']
    argfile = open("arglist.txt","w")
    full_iter_data=[]
    arg_list = [([(k, v) for v in vs]) for (k, (n, vs)) in args.items() if vs is not None]
    argfile.write(str(args))
    argfile.write(str(arg_list))
    argfile.write("\n")
    arg_product = list(it.product(*arg_list))
    argfile.write(str(arg_product))
    argfile.close()
    one_hot = OneHotEncoder()

    y_train= one_hot.fit_transform(y_train.reshape(-1, 1)).todense()
    y_test = one_hot.transform(y_test.reshape(-1, 1)).todense()
    
    for arg_value in arg_product:
        arg_dict = dict(arg_value)
        nn_model1 = mlrose.NeuralNetwork(
                                    bias = True, is_classifier = True,
                                    early_stopping = True,
                                    random_state = 10, **arg_dict)
        scores = cross_validate(nn_model1, X_train, y_train, scoring=scoring,cv=5, n_jobs=4)
        print(sorted(scores.keys()))
        print(scores.values())
        iter_data = {}
        for i in scores.keys():
            iter_data[i] = np.mean(scores[i])           
        iter_data.update(arg_dict)
        full_iter_data.append(iter_data)
    df_iter_data = pd.DataFrame(full_iter_data)
    timestr = time.strftime("%Y%m%d-%H%M%S")
    filename = "./NN_" + timestr +".csv"
    df_iter_data.to_csv(filename)


def main():
    full_iter_data=[]
    #generate classification data
    X,y = generate_data()
    X_train, X_test, y_train, y_test = preprocess_data(X,y)
    scaler = StandardScaler()

    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    one_hot = OneHotEncoder()

    y_train= one_hot.fit_transform(y_train.reshape(-1, 1)).todense()
    y_test = one_hot.transform(y_test.reshape(-1, 1)).todense()

    activation_arr = ['relu', 'tanh']
    hidden_nodes = [[20]]#,[40],[20,20],[20,20,20]]
    niter=[1500,2000]
    learning_rate = [0.1,0.01]
    max_attempts=[20]

    algo=['simulated_annealing']

    if("simulated_annealing" in algo):
        print("here")
        decay_type_list = [mlrose.GeomDecay, mlrose.ExpDecay, mlrose.ArithDecay]
        expdecay_range_list = list(np.arange(0.005,0.1,0.005))
        arithdecay_range_list = list(np.arange(0.0001,0.1,0.01))
        geomdecay_range_list = list(np.arange(0.1,0.99,0.09))
        decay_obj_list = [decay_type_list[0](decay=t) for t in geomdecay_range_list] + \
                        [decay_type_list[1](exp_const=t) for t in expdecay_range_list] #+ \
                        #[decay_type_list[2](decay=t) for t in arithdecay_range_list]


        gridsearch(X_train, y_train, X_test,y_test, max_iters=('niters', niter),
                                                    learning_rate=('learning_rate', learning_rate),
                                                    algorithm=('algo', algo),
                                                    hidden_nodes =('hidden_nodes', hidden_nodes) ,
                                                    activation=('activation',activation_arr),
                                                    schedule=('Decay', decay_obj_list), 
                                                    max_attempts=('max_attempts', max_attempts))
     
    algo = ['random_hill_climb']
    if("random_hill_climb" in algo):
        restarts = [0,1,5,10]
        gridsearch(X_train, y_train, X_test,y_test, max_iters=('niters', niter),
                                                    learning_rate=('learning_rate', learning_rate),
                                                    algorithm=('algo', algo),
                                                    hidden_nodes =('hidden_nodes', hidden_nodes) ,
                                                    activation=('activation',activation_arr),                                                    
                                                    max_attempts=('max_attempts', max_attempts),
                                                    restarts = ('restarts',restarts))
    algo = ['genetic_alg']
    if("genetic_alg" in algo):
        pop_size=[200,300,400,500,1000]
        mutation_prob = [0.1,0.2,0.3]
        gridsearch(X_train, y_train, X_test,y_test, max_iters=('niters', niter),
                                                    learning_rate=('learning_rate', learning_rate),
                                                    algorithm=('algo', algo),
                                                    hidden_nodes =('hidden_nodes', hidden_nodes) ,
                                                    activation=('activation',activation_arr),                                                    
                                                    max_attempts=('max_attempts', max_attempts),
                                                    mutation_prob = ('mutation_prob',mutation_prob),
                                                    pop_size = ('pop_size',pop_size))
    algo = ['gradient_descent']
    if("gradient_descent" in algo):
        gridsearch(X_train, y_train, X_test,y_test, max_iters=('niters', niter),
                                                    learning_rate=('learning_rate', learning_rate),
                                                    algorithm=('algo', algo),
                                                    hidden_nodes =('hidden_nodes', hidden_nodes) ,
                                                    activation=('activation',activation_arr),                                                    
                                                    max_attempts=('max_attempts', max_attempts))



            
def preprocess_data(X,y):    
    #preprocess data in training
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=122, stratify=y, shuffle=True)
    return X_train, X_test, y_train, y_test

def generate_data():
    np.random.seed(5000)

    X, y = make_classification(n_samples=4000, n_features=5, n_informative=4, 
                                n_redundant=1, n_repeated=0, n_classes=3, 
                                n_clusters_per_class=2, weights=[0.4,0.3,0.3], flip_y=0.05, 
                                class_sep=1.0, hypercube=True, shift=0.0, scale=1.0, 
                                shuffle=True, random_state=50)

    return X,y

    
if __name__ == '__main__':
    main()